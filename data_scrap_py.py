import time
import json

from selenium import webdriver
from selenium.webdriver.common.keys import Keys

urls = ["https://play.google.com/store/apps/collection/recommended_for_you?clp=ogoGCAEqAggB:S:ANO1ljLRwrE"] 
data = []
browser = webdriver.Chrome("C:/Users/u1125771/Downloads/chromedriver.exe")

for url in urls: 
    browser.get(url)
    time.sleep(1)

    elem = browser.find_element_by_tag_name("body")

    no_of_pagedowns = 50

    while no_of_pagedowns:
        elem.send_keys(Keys.PAGE_DOWN)
        time.sleep(0.2)
        no_of_pagedowns-=1

    post_elems = browser.find_elements_by_class_name("card-click-target")
    #print post_elems[0].get_attribute('href')
    for post in post_elems:
        data.append(post.get_attribute('href'))
print("Toatl apps:", len(data))
with open('data_py.json','w') as output_file:
    json.dump(data, output_file)